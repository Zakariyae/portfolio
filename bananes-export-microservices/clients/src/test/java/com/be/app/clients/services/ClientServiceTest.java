package com.be.app.clients.services;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.be.app.clients.domains.Address;
import com.be.app.clients.domains.Client;
import com.be.app.clients.dtos.AddressDto;
import com.be.app.clients.dtos.ClientDto;
import com.be.app.clients.repositories.ClientRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class ClientServiceTest {

    @Mock
    private Mapper mapper;

    @Mock
    private ClientRepository repository;

    @InjectMocks
    private ClientService service;

    private static final Client client =
            new Client(1L, "Zakariyae",
                    new Address(1L, "5 rue besson", 94110, "Arcueil", "France"));

    @Test
    void shouldReturnClientDtoById() {
        long id = 1;
        ClientDto mappedClient = map(client);
        when(repository.findById(anyLong())).thenReturn(java.util.Optional.of(client));
        when(mapper.map(ArgumentMatchers.<Optional<Client>>any(), ArgumentMatchers.any())).thenReturn(mappedClient);

        Optional<ClientDto> clientDto = service.findById(id);

        assertThat(clientDto).isPresent().get().isSameAs(mappedClient);
    }

    @Test
    void shouldReturnClientDtos() {
        List<ClientDto> mappedClients = clientDtos();
        when(repository.findAll(ArgumentMatchers.<Specification<Client>>any())).thenReturn(Collections.singletonList(client));
        when(mapper.mapCollection(ArgumentMatchers.any(), ArgumentMatchers.any())).thenAnswer(invocation -> mappedClients);

        List<ClientDto> clientDtos = service.findAll(null);

        assertThat(clientDtos).isNotEmpty().containsExactlyElementsOf(mappedClients);
    }

    ClientDto map(Client client) {
        return new ClientDto(
                client.getId(),
                client.getName(),
                new AddressDto(
                        client.getAddress().getId(),
                        client.getAddress().getStreet(),
                        client.getAddress().getZipCode(),
                        client.getAddress().getCity(),
                        client.getAddress().getCountry()
                ));
    }

    List<ClientDto> clientDtos() {
        return Stream.of(client).map(this::map).collect(Collectors.toList());
    }

}
