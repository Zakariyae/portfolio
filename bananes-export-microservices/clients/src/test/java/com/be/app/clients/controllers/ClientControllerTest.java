package com.be.app.clients.controllers;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.be.app.clients.domains.Address;
import com.be.app.clients.domains.Client;
import com.be.app.clients.dtos.AddressDto;
import com.be.app.clients.dtos.ClientDto;
import com.be.app.clients.services.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class ClientControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper jsonMapper;

    @Mock
    private ClientService service;

    @InjectMocks
    private ClientController controller;

    @BeforeEach
    void init() {
        jsonMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    private static final Client client =
            new Client(1L, "Zakariyae",
                    new Address(1L, "5 rue besson", 94110, "Arcueil", "France"));

    @Test
    void shouldReturnAllClients() throws Exception {
        List<ClientDto> clientDtos = clientDtos();
        when(service.findAll(ArgumentMatchers.<Specification<Client>>any())).thenReturn(clientDtos);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/clients/all")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString()).isNotNull().isEqualTo(jsonMapper.writeValueAsString(clientDtos));
    }

    @Test
    void shouldReturnClientById() throws Exception {
        ClientDto clientDto = map(client);
        when(service.findById(ArgumentMatchers.anyLong())).thenReturn(Optional.of(clientDto));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/clients/1")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString()).isNotNull().isEqualTo(jsonMapper.writeValueAsString(clientDto));
    }

    ClientDto map(Client client) {
        return new ClientDto(
                client.getId(),
                client.getName(),
                new AddressDto(
                        client.getAddress().getId(),
                        client.getAddress().getStreet(),
                        client.getAddress().getZipCode(),
                        client.getAddress().getCity(),
                        client.getAddress().getCountry()
                ));
    }

    List<ClientDto> clientDtos() {
        return Stream.of(client).map(this::map).collect(Collectors.toList());
    }

}
