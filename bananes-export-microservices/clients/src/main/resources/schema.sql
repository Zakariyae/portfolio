CREATE TABLE Destinataire (
    id numeric identity not null,
    nom varchar(255) null,
    id_adresse numeric null
);

CREATE TABLE Adresse (
    id numeric identity not null,
    adresse varchar(255) null,
    code_postal int null,
    ville varchar(255) null,
    pays varchar(255) null
);

ALTER TABLE Destinataire ADD FOREIGN KEY (id_adresse) REFERENCES Adresse (id);