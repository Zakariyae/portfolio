package com.be.app.clients.repositories;

import com.be.app.clients.domains.Client;

public interface ClientRepository extends JpaSpecificationRepository<Client, Long> {
}
