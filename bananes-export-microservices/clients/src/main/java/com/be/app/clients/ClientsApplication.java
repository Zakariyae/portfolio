package com.be.app.clients;

import com.be.app.clients.utils.DozerFieldMapper;
import org.dozer.DozerBeanMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@EnableSwagger2
@SpringBootApplication
public class ClientsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientsApplication.class, args);
    }

    @Bean
    public DozerBeanMapper dozerBeanMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();

        mapper.setMappingFiles(Collections.singletonList("dozer-dto-mapper.xml"));
        mapper.setCustomFieldMapper(new DozerFieldMapper());

        return mapper;
    }

}
