package com.be.app.clients.utils;

import org.dozer.CustomFieldMapper;
import org.dozer.classmap.ClassMap;
import org.dozer.fieldmap.FieldMap;
import org.hibernate.Hibernate;

public class DozerFieldMapper implements CustomFieldMapper {

    public boolean mapField(Object source, Object destination, Object sourceFieldValue, ClassMap classMap,
                            FieldMap fieldMapping) {
        return source != null && sourceFieldValue != null
                && !Hibernate.isInitialized(sourceFieldValue);
    }

}