package com.be.app.clients.services;

import com.be.app.clients.domains.Client;
import com.be.app.clients.dtos.ClientDto;
import com.be.app.clients.repositories.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClientService {

    private Mapper mapper;

    private ClientRepository repository;

    public Optional<ClientDto> findById(Long id) {
        return Optional.of(mapper.map(repository.findById(id).orElseThrow(() -> new RuntimeException("client not found")), ClientDto.class));
    }

    public List<ClientDto> findAll(Specification<Client> specification) {
        return mapper.mapCollection(repository.findAll(specification), ClientDto.class);
    }

}
