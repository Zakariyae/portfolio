package com.be.app.clients.controllers;

import com.be.app.clients.dtos.ClientDto;
import com.be.app.clients.services.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/clients")
public class ClientController {

    private ClientService service;

    @GetMapping("all")
    public ResponseEntity<List<ClientDto>> findAll() {
        return new ResponseEntity<>(service.findAll(null), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<ClientDto> findById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id).orElse(new ClientDto()), HttpStatus.OK);
    }

}
