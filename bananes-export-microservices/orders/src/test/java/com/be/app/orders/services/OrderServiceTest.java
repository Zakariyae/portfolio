package com.be.app.orders.services;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.be.app.orders.domain.Order;
import com.be.app.orders.dtos.ClientDto;
import com.be.app.orders.dtos.OrderDto;
import com.be.app.orders.repositories.OrderRepository;
import com.be.app.orders.service.Mapper;
import com.be.app.orders.service.OrderService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class OrderServiceTest {

    @Mock
    private Mapper mapper;

    @Mock
    private OrderRepository repository;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private OrderService service;

    private static final Order order =
            new Order(1L, new Date(), 3, 1L);

    @Test
    void shouldReturnClientOrders() {
        List<OrderDto> mappedOrders = orderDtos();
        when(repository.findAll(ArgumentMatchers.<Specification<Order>>any()))
                .thenReturn(Collections.singletonList(order));
        when(mapper.mapCollection(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenAnswer(invocation -> mappedOrders);
        when(restTemplate.getForObject(ArgumentMatchers.anyString(), ArgumentMatchers.any()))
                .thenReturn(new ClientDto(1L, "Zakariyae", null));

        List<OrderDto> orderDtos = service.findByClientId(1L);

        assertThat(orderDtos.stream()
                            .map(orderDto -> orderDto.getId() + " " + orderDto.getClientName())
                            .collect(Collectors.toList()))
                .isNotEmpty()
                .containsExactlyElementsOf(
                        mappedOrders.stream()
                                    .map(orderDto -> orderDto.getId() + " " + orderDto.getClientName())
                                    .collect(Collectors.toList())
                );
    }

    OrderDto map(Order order) {
        return new OrderDto(
                order.getId(),
                order.getDeliveryDate(),
                order.getQuantity(),
                order.getQuantity() * 2.5,
                "Zakariyae");
    }

    List<OrderDto> orderDtos() {
        return Stream.of(order).map(this::map).collect(Collectors.toList());
    }

}
