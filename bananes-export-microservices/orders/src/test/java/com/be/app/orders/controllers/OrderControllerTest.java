package com.be.app.orders.controllers;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.be.app.orders.domain.Order;
import com.be.app.orders.dtos.OrderDto;
import com.be.app.orders.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class OrderControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper jsonMapper;

    @Mock
    private OrderService service;

    @InjectMocks
    private OrderController controller;

    private static final Order order =
            new Order(1L, new Date(), 3, 1L);

    @BeforeEach
    void init() {
        jsonMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void shouldReturnClientOrders() throws Exception {
        List<OrderDto> orderDtos = orderDtos();
        when(service.findByClientId(anyLong())).thenReturn(orderDtos);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
                .get("/orders/client/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        assertThat(result.getResponse().getContentAsString()).isNotNull().isEqualTo(jsonMapper.writeValueAsString(orderDtos));
    }

    OrderDto map(Order order) {
        return new OrderDto(
                order.getId(),
                order.getDeliveryDate(),
                order.getQuantity(),
                order.getQuantity() * 2.5,
                "Zakariyae");
    }

    List<OrderDto> orderDtos() {
        return Stream.of(order).map(this::map).collect(Collectors.toList());
    }

}
