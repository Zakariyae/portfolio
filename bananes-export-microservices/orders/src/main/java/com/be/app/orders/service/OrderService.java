package com.be.app.orders.service;

import com.be.app.orders.dtos.ClientDto;
import com.be.app.orders.dtos.OrderDto;
import com.be.app.orders.repositories.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OrderService {

    private static final Double KG_PRICE = 2.5D;

    private Mapper mapper;

    private OrderRepository repository;

    private RestTemplate restTemplate;

    public List<OrderDto> findByClientId(Long id) {
        return mapper.mapCollection(
                repository.findAll((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("clientId"), id)),
                OrderDto.class
        ).stream()
                .map(orderDto -> {
                    ClientDto clientDto = restTemplate.getForObject("http://localhost:9090/clients/".concat(String.valueOf(id)), ClientDto.class);
                    return new OrderDto(orderDto.getId(),
                            orderDto.getDeliveryDate(),
                            orderDto.getQuantity(),
                            orderDto.getQuantity() * KG_PRICE,
                            clientDto != null ? clientDto.getName() : "Not found");
                })
                .collect(Collectors.toList());
    }

}
