package com.be.app.orders.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;

@NoRepositoryBean
public interface JpaSpecificationRepository<T, Z extends Serializable>
        extends PagingAndSortingRepository<T, Z>, JpaSpecificationExecutor<T> {
}