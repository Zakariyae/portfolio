package com.be.app.orders.repositories;

import com.be.app.orders.domain.Order;

public interface OrderRepository extends JpaSpecificationRepository<Order, Long> {
}
