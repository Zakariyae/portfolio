package com.be.app.orders.service;

import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class Mapper {

    DozerBeanMapper dozerBeanMapper;

    public <T> T map(Object sourceObject, Class<T> destinationClass) {
        return dozerBeanMapper.map(sourceObject, destinationClass);
    }

    public <T> List<T> mapCollection(List<?> sourceCollection, Class<T> destinationClass) {
        List<T> destinationCollection = new ArrayList<>();

        if (!sourceCollection.isEmpty())
            sourceCollection.forEach(element -> destinationCollection.add(dozerBeanMapper.map(element, destinationClass)));

        return destinationCollection;
    }

}
