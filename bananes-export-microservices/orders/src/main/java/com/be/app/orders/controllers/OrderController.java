package com.be.app.orders.controllers;

import com.be.app.orders.dtos.OrderDto;
import com.be.app.orders.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("orders")
public class OrderController {

    private OrderService service;

    @GetMapping("client/{id}")
    public ResponseEntity<List<OrderDto>> findByClient(@PathVariable Long id) {
        return new ResponseEntity<>(service.findByClientId(id), HttpStatus.OK);
    }

}
