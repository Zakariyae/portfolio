CREATE TABLE Commande (
    id numeric identity not null,
    date_livraison date null,
    quantite_bananes int null,
    prix double null,
    id_destinataire numeric null
);