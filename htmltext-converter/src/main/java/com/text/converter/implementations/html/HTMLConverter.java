package com.text.converter.implementations.html;

import com.text.converter.Converter;
import com.text.converter.exceptions.IllegalTextException;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Service;

@Service
public class HTMLConverter implements Converter {

    /**
     * {@inheritDoc}
     */
    public String convert(String text) {
        if (text == null || text.isEmpty())
            throw new IllegalTextException("Illegal Text content: " + text);

        return encodeHTMLChars(text);
    }

    /**
     * encode html chars containing in text
     *
     * @param text - text to be converted to HTML
     * @return an HTML content
     */
    private String encodeHTMLChars(String text) {
        return "<html><body><div>".concat(StringEscapeUtils.escapeHtml4(text))
                .replace(System.lineSeparator(), "<br/>").concat("</div></body></html>");
    }

}
