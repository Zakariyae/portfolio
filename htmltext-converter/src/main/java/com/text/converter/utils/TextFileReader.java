package com.text.converter.utils;

import com.google.common.io.Resources;
import lombok.extern.slf4j.Slf4j;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class TextFileReader {

    private TextFileReader() {
    }

    public static List<String> read(String[] fileNames) {
        return Arrays.stream(fileNames)
                .map(fileName -> {
                    String textContent = null;
                    try (Stream<String> stream = Files.lines(Paths.get(Resources.getResource(fileName).toURI()))) {
                        textContent = stream.collect(Collectors.joining(System.lineSeparator()));
                    } catch (Exception e) {
                        log.error("Error during reading text files : {}", e.getMessage());
                    }

                    return textContent;
                })
                .collect(Collectors.toList());
    }

}
