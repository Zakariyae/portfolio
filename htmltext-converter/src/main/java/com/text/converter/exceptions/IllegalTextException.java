package com.text.converter.exceptions;

public class IllegalTextException extends RuntimeException {

    private static final long serialVersionUID = 10;

    public IllegalTextException(String message) {
        super(message);
    }

}
