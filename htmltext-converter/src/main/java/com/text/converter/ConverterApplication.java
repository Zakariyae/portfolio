package com.text.converter;

import com.text.converter.implementations.html.HTMLConverter;
import com.text.converter.utils.TextFileReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.List;

@Slf4j
@SpringBootApplication
public class ConverterApplication {

    public static void main(String[] args) {
        // Spring applicationContext
        ApplicationContext ctx = SpringApplication.run(ConverterApplication.class, args);

        // HTML converter
        Converter htmlConverter = ctx.getBean(HTMLConverter.class);

        // Read example text files
        List<String> filesContent = TextFileReader.read(new String[]{"fileOne.txt", "fileTwo.txt", "fileThree.txt"});

        filesContent.forEach(fileContent -> log.info("HTML Content: {}", htmlConverter.convert(fileContent)));
    }

}
