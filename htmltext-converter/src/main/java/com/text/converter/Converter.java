package com.text.converter;

public interface Converter {

    /**
     * convert text to the specified language
     *
     * @param text - the text to be converted
     * @return the converted text
     */
    String convert(String text);

}
