package com.text.converter;

import static org.assertj.core.api.Assertions.*;

import com.text.converter.exceptions.IllegalTextException;
import com.text.converter.implementations.html.HTMLConverter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HTMLConverterTests {

    private final Converter htmlConverter;

    public HTMLConverterTests() {
        this.htmlConverter = new HTMLConverter();
    }

    @Test
    void shouldThrowExceptionForNullOrEmptyValues() {
        assertThatThrownBy(() -> htmlConverter.convert(null))
                .isInstanceOf(IllegalTextException.class)
                .hasMessageContaining("Illegal Text content: null");

        assertThatThrownBy(() -> htmlConverter.convert(""))
                .isInstanceOf(IllegalTextException.class)
                .hasMessageContaining("Illegal Text content: ");
    }


    @Test
    void shouldConvertAmpersand() {
        String textContent = "Zakariyae HADDINI & I have 28 years old.";
        String htmlContent = "<html><body><div>Zakariyae HADDINI &amp; I have 28 years old.</div></body></html>";

        assertThat(htmlConverter.convert(textContent)).isEqualTo(htmlContent);
    }

    @Test
    void shouldConvertGreatethanAndLessthan() {
        String textContent = "12 > 3 & 3 < 12";
        String htmlContent = "<html><body><div>12 &gt; 3 &amp; 3 &lt; 12</div></body></html>";

        assertThat(htmlConverter.convert(textContent)).isEqualTo(htmlContent);
    }

    @Test
    void shouldAddBreaklineForMultipleLines() {
        String textContent = "Zakariyae HADDINI".concat(System.lineSeparator())
                .concat("Ingénieur Développeur Java/JavaEE/Angular")
                .concat(System.lineSeparator())
                .concat("28 years old.");
        String htmlContent = "<html><body><div>Zakariyae HADDINI<br/>Ing&eacute;nieur D&eacute;veloppeur Java/JavaEE/Angular<br/>28 years old.</div></body></html>";

        assertThat(htmlConverter.convert(textContent)).isEqualTo(htmlContent);
    }

}
