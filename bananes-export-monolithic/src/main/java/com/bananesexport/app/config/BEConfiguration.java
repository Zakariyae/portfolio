package com.bananesexport.app.config;

import com.bananesexport.app.utils.DozerFieldMapper;
import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class BEConfiguration {

    @Bean
    public DozerBeanMapper dozerBeanMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();

        mapper.setMappingFiles(Collections.singletonList("dozer-dto-mapper.xml"));
        mapper.setCustomFieldMapper(new DozerFieldMapper());

        return mapper;
    }

}
