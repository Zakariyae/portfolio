package com.bananesexport.app.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface CrudService<E, R, Z> {

    R findById(Specification<E> specification);

    R saveOrUpdate(R entity);

    void delete(Z id);

    List<R> findAll(Specification<E> specification);

    Page<R> findAllPaginated(Specification<E> specification, Pageable pageable);

}
