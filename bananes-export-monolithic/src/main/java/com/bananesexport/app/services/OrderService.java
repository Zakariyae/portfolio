package com.bananesexport.app.services;

import com.bananesexport.app.domains.Order;
import com.bananesexport.app.dtos.OrderDto;
import com.bananesexport.app.repositories.OrderRepository;
import com.bananesexport.app.utils.Mapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OrderService implements CrudService<Order, OrderDto, Long> {

    //private static final Double PRICE = 2.5D;

    private Mapper mapper;
    private OrderRepository repository;

    @Override
    public OrderDto findById(Specification<Order> specification) {
        return mapper.map(repository.findOne(specification).orElseThrow(() -> new RuntimeException("Order not found")), OrderDto.class);
    }

    @Override
    public OrderDto saveOrUpdate(OrderDto orderDto) {
        //orderDto.setPrice(orderDto.getQuantity() * PRICE);
        return mapper.map(repository.save(mapper.map(orderDto, Order.class)), OrderDto.class);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<OrderDto> findAll(Specification<Order> specification) {
        return mapper.mapCollection(repository.findAll(specification), OrderDto.class);
    }

    @Override
    public Page<OrderDto> findAllPaginated(Specification<Order> specification, Pageable pageable) {
        Page<Order> page = repository.findAll(specification, pageable);
        return new PageImpl<>(mapper.mapCollection(page.getContent(), OrderDto.class), pageable, page.getTotalElements());
    }

}
