package com.bananesexport.app.services;

import com.bananesexport.app.domains.Client;
import com.bananesexport.app.dtos.ClientDto;
import com.bananesexport.app.repositories.ClientRepository;
import com.bananesexport.app.utils.Mapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ClientService implements CrudService<Client, ClientDto, Long> {

    private Mapper mapper;
    private ClientRepository repository;

    @Override
    public ClientDto findById(Specification<Client> specification) {
        return mapper.map(repository.findOne(specification).orElseThrow(() -> new RuntimeException("Client not found")), ClientDto.class);
    }

    @Override
    public ClientDto saveOrUpdate(ClientDto clientDto) {
        return mapper.map(repository.save(mapper.map(clientDto, Client.class)), ClientDto.class);
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<ClientDto> findAll(Specification<Client> specification) {
        return mapper.mapCollection(repository.findAll(specification), ClientDto.class);
    }

    @Override
    public Page<ClientDto> findAllPaginated(Specification<Client> specification, Pageable pageable) {
        Page<Client> page = repository.findAll(specification, pageable);
        return new PageImpl<>(mapper.mapCollection(page.getContent(), ClientDto.class), pageable, page.getTotalElements());
    }

}
