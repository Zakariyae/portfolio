package com.bananesexport.app.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Commande")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_livraison")
    private Date deliveryDate;

    @Column(name = "quantite_bananes")
    private Integer quantity;

//    @Transient
//    @Column(name = "prix")
//    private Double price;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "id_destinataire")
    private Client client;

    public Double calculatePrice() {
        return quantity * 2.5D;
    }

}
