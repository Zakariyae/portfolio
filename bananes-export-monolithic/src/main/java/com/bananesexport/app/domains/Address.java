package com.bananesexport.app.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Adresse")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "adresse")
    private String street;

    @Column(name = "code_postal")
    private Integer zipCode;

    @Column(name = "ville")
    private String city;

    @Column(name = "pays")
    private String country;

}
