package com.bananesexport.app.utils;

import lombok.AllArgsConstructor;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class Mapper {

    DozerBeanMapper dozerBeanMapper;

    public <T> T map(Object sourceObject, Class<T> destinationClass) {
        return dozerBeanMapper.map(sourceObject, destinationClass);
    }

    public <T> List<T> mapCollection(List<?> sourceCollection, Class<T> destinationClass) {
//        List<T> destinationCollection = new ArrayList<>();
//
//        if (!sourceCollection.isEmpty())
//            sourceCollection.forEach(element -> destinationCollection.add(dozerBeanMapper.map(element, destinationClass)));

        return sourceCollection.stream().map(source -> dozerBeanMapper.map(source, destinationClass)).collect(Collectors.toList());
    }

}
