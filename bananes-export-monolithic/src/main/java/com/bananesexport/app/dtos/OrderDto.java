package com.bananesexport.app.dtos;

import com.bananesexport.app.validators.annotations.ClientConstraint;
import com.bananesexport.app.validators.annotations.DeliveryDateConstraint;
import com.bananesexport.app.validators.annotations.QuantityConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private Long id;

    @DeliveryDateConstraint
    private Date deliveryDate;

    @QuantityConstraint
    private Integer quantity;

    private Double price;

    @ClientConstraint
    private ClientDto client;

}
