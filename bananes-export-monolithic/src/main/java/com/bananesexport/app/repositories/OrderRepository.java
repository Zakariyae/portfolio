package com.bananesexport.app.repositories;

import com.bananesexport.app.domains.Order;

public interface OrderRepository extends JpaSpecificationRepository<Order, Long> {
}
