package com.bananesexport.app.repositories;

import com.bananesexport.app.domains.Client;

public interface ClientRepository extends JpaSpecificationRepository<Client, Long> {
}
