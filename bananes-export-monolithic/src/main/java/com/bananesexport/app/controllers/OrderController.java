package com.bananesexport.app.controllers;

import static com.bananesexport.app.specifications.OrderSpecification.*;

import com.bananesexport.app.domains.Order;
import com.bananesexport.app.dtos.OrderDto;
import com.bananesexport.app.services.CrudService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/orders")
public class OrderController {

    private CrudService<Order, OrderDto, Long> service;

    @GetMapping("{id}")
    public ResponseEntity<OrderDto> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.findById(orderJoinSpecification().and(orderIdSpecification(id))), HttpStatus.OK);
    }

    @GetMapping("client/{id}")
    public ResponseEntity<List<OrderDto>> findByClientId(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.findAll(orderJoinSpecification()
                .and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("client").get("id"), id))),
                HttpStatus.OK);
    }

    @GetMapping("client/{id}/paginated")
    public ResponseEntity<Page<OrderDto>> findByClientId(@PathVariable("id") Long id,
                                                         @RequestParam(required = false, defaultValue = "0") Integer page,
                                                         @RequestParam(required = false, defaultValue = "10") Integer size) {
        return new ResponseEntity<>(service.findAllPaginated(orderJoinSpecification()
                .and((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("client").get("id"), id)), PageRequest.of(page, size)),
                HttpStatus.OK);
    }

    @PostMapping("add")
    public ResponseEntity<OrderDto> saveOrder(@Valid @RequestBody OrderDto order) {
        return new ResponseEntity<>(service.saveOrUpdate(order), HttpStatus.OK);
    }

    @PutMapping("edit")
    public ResponseEntity<OrderDto> updateOrder(@Valid @RequestBody OrderDto order) {
        return new ResponseEntity<>(service.saveOrUpdate(order), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteOrder(@PathVariable("id") Long id) {
        service.delete(id);
    }

}
