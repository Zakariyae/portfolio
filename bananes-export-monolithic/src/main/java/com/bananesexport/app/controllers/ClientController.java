package com.bananesexport.app.controllers;

import static com.bananesexport.app.specifications.ClientSpecification.*;

import com.bananesexport.app.domains.Client;
import com.bananesexport.app.dtos.ClientDto;
import com.bananesexport.app.services.CrudService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/clients")
public class ClientController {

    private final CrudService<Client, ClientDto, Long> service;

    @GetMapping("{id}")
    public ResponseEntity<ClientDto> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.findById(clientJoinSpecification().and(clientIdSpecification(id))), HttpStatus.OK);
    }

    @GetMapping("all")
    public ResponseEntity<List<ClientDto>> findAll() {
        return new ResponseEntity<>(service.findAll(clientJoinSpecification()), HttpStatus.OK);
    }

    @GetMapping("all/paginated")
    public ResponseEntity<Page<ClientDto>> findAllPaginated(@RequestParam(required = false, defaultValue = "0") Integer page,
                                                            @RequestParam(required = false, defaultValue = "10") Integer size) {
        return new ResponseEntity<>(service.findAllPaginated(clientJoinSpecification(), PageRequest.of(page, size)), HttpStatus.OK);
    }

    @PostMapping("add")
    public ResponseEntity<ClientDto> saveClient(@Valid @RequestBody ClientDto client) {
        return new ResponseEntity<>(service.saveOrUpdate(client), HttpStatus.OK);
    }

    @PutMapping("edit")
    public ResponseEntity<ClientDto> updateClient(@RequestBody ClientDto client) {
        return new ResponseEntity<>(service.saveOrUpdate(client), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteClient(@PathVariable("id") Long id) {
        service.delete(id);
    }

}
