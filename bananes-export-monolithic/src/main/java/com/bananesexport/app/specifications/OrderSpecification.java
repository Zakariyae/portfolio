package com.bananesexport.app.specifications;

import com.bananesexport.app.domains.Order;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.JoinType;

public class OrderSpecification {

    private OrderSpecification() {
    }

    public static Specification<Order> orderIdSpecification(Long id) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
    }

    public static Specification<Order> orderJoinSpecification() {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (Long.class != criteriaQuery.getResultType()) {
                root.fetch("client").fetch("address", JoinType.INNER);
            }

            return criteriaBuilder.conjunction();
        };
    }

}
