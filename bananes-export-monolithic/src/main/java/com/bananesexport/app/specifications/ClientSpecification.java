package com.bananesexport.app.specifications;

import com.bananesexport.app.domains.Client;
import com.bananesexport.app.dtos.ClientDto;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.JoinType;

public class ClientSpecification {

    private ClientSpecification() {
    }

    public static Specification<Client> clientIdSpecification(Long id) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
    }

    public static Specification<Client> clientAllFieldsSpecification(ClientDto clientDto) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.and(
                criteriaBuilder.equal(root.get("name"), clientDto.getName()),
                criteriaBuilder.equal(root.get("address").get("street"), clientDto.getAddress().getStreet()),
                criteriaBuilder.equal(root.get("address").get("zipCode"), clientDto.getAddress().getZipCode()),
                criteriaBuilder.equal(root.get("address").get("city"), clientDto.getAddress().getCity()),
                criteriaBuilder.equal(root.get("address").get("country"), clientDto.getAddress().getCountry())
        );
    }

    public static Specification<Client> clientJoinSpecification() {
        return (root, criteriaQuery, criteriaBuilder) -> {
            if (Long.class != criteriaQuery.getResultType()) {
                root.fetch("address", JoinType.INNER);
            }

            return criteriaBuilder.conjunction();
        };
    }

}
