package com.bananesexport.app.validators;

import com.bananesexport.app.validators.annotations.DeliveryDateConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DeliveryDateValidator implements
        ConstraintValidator<DeliveryDateConstraint, Date> {

    @Override
    public boolean isValid(Date deliveryDate, ConstraintValidatorContext constraintValidatorContext) {
        return deliveryDate != null &&
                deliveryDate.after(Date.from(LocalDate.now().plusDays(7).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
    }

}