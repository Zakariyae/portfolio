package com.bananesexport.app.validators;

import com.bananesexport.app.validators.annotations.QuantityConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuantityValidator implements ConstraintValidator<QuantityConstraint, Integer> {

    @Override
    public boolean isValid(Integer quantity, ConstraintValidatorContext constraintValidatorContext) {
        return quantity != null && quantity > 0 && quantity <= 10000 && quantity % 25 == 0;
    }

}
