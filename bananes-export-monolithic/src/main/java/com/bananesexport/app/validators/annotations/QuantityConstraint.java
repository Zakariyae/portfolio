package com.bananesexport.app.validators.annotations;

import com.bananesexport.app.validators.QuantityValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = QuantityValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface QuantityConstraint {
    String message() default "Quantity value must be between 0 and 10000 and a multiple of 25";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
