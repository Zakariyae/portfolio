package com.bananesexport.app.validators.annotations;

import com.bananesexport.app.validators.ClientValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ClientValidator.class)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ClientConstraint {
    String message() default "Client already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
