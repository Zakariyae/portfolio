package com.bananesexport.app.validators;

import static com.bananesexport.app.specifications.ClientSpecification.*;

import com.bananesexport.app.domains.Client;
import com.bananesexport.app.dtos.ClientDto;
import com.bananesexport.app.services.CrudService;
import com.bananesexport.app.validators.annotations.ClientConstraint;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

@AllArgsConstructor
public class ClientValidator implements ConstraintValidator<ClientConstraint, ClientDto> {

    private CrudService<Client, ClientDto, Long> service;

    @Override
    public boolean isValid(ClientDto clientDto, ConstraintValidatorContext constraintValidatorContext) {
        try {
            List<ClientDto> clientDtos = service.findAll(clientJoinSpecification().and(clientAllFieldsSpecification(clientDto)));
            return clientDtos.isEmpty() || (clientDtos.size() == 1 && !clientDto.equals(clientDtos.get(0)));
        } catch (Exception e) {
            return false;
        }
    }

}
