package com.bananesexport.app.validators.annotations;

import com.bananesexport.app.validators.DeliveryDateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DeliveryDateValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DeliveryDateConstraint {
    String message() default "Delivery date must be at least greater by one week than today's date";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
