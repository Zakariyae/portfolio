package com.bananesexport.app.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import com.bananesexport.app.dtos.AddressDto;
import com.bananesexport.app.dtos.ClientDto;
import com.bananesexport.app.services.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ClientControllerTest {

    @Mock
    private ClientService service;

    @InjectMocks
    private ClientController controller;

    private MockMvc mockMvc;
    private ObjectMapper jsonMapper;
    private List<ClientDto> clientDtos;

    @BeforeEach
    void init() {
        clientDtos = clientDtos();
        jsonMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(controller).setValidator(mock(Validator.class)).build();
    }

    @Test
    void findById() throws Exception {
        when(service.findById(ArgumentMatchers.any()))
                .thenReturn(
                        clientDtos.stream().filter(c -> c.getId() == 1L).findFirst().orElse(null)
                );

        MvcResult mvcResult = mockMvc.perform(
                get("/clients/1").accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(clientDtos.stream().filter(c -> c.getId() == 1L).findFirst().orElse(null)));
    }

    @Test
    void findAll() throws Exception {
        when(service.findAll(ArgumentMatchers.any()))
                .thenReturn(clientDtos);

        MvcResult mvcResult = mockMvc.perform(
                get("/clients/all").accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(clientDtos));
    }

    @Test
    void findAllPaginated() throws Exception {
        when(service.findAllPaginated(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(
                        new PageImpl<>(clientDtos.subList(0, 2), PageRequest.of(0, 2), clientDtos.size())
                );

        MvcResult mvcResult = mockMvc.perform(
                get("/clients/all/paginated")
                        .param("page", String.valueOf(0))
                        .param("size", String.valueOf(2))
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(new PageImpl<>(clientDtos.subList(0, 2), PageRequest.of(0, 2), clientDtos.size())));
    }

    @Test
    void saveClient() throws Exception {
        ClientDto clientDto = new ClientDto(0L, "Zakariyae", new AddressDto(0L, "5 rue besson", 94110, "Arcueil", "France"));

        clientDto.setId(null);
        clientDto.getAddress().setId(null);

        when(service.saveOrUpdate(ArgumentMatchers.any()))
                .thenReturn(
                        clientDtos.stream().filter(c -> c.getId() == 1L).findFirst().orElse(null)
                );

        MvcResult mvcResult = mockMvc.perform(
                post("/clients/add")
                        .content(jsonMapper.writeValueAsString(clientDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(clientDtos.stream().filter(c -> c.getId() == 1L).findFirst().orElse(null)));
    }

    List<ClientDto> clientDtos() {
        List<ClientDto> list = new ArrayList<>();

        list.add(new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France")));
        list.add(new ClientDto(2L, "Kamel", new AddressDto(2L, "10 rue besson", 60000, "Oujda", "Maroc")));
        list.add(new ClientDto(3L, "Oualid", new AddressDto(3L, "15 rue besson", 68000, "Oujda", "Maroc")));
        list.add(new ClientDto(4L, "Amine", new AddressDto(4L, "20 rue besson", 75001, "Paris", "France")));

        return list;
    }

}
