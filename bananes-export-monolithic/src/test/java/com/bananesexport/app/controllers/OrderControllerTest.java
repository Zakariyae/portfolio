package com.bananesexport.app.controllers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import com.bananesexport.app.dtos.AddressDto;
import com.bananesexport.app.dtos.ClientDto;
import com.bananesexport.app.dtos.OrderDto;
import com.bananesexport.app.services.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class OrderControllerTest {

    @Mock
    private OrderService service;

    @InjectMocks
    private OrderController controller;

    private MockMvc mockMvc;
    private ObjectMapper jsonMapper;
    private List<OrderDto> orderDtos;

    @BeforeEach
    void init() {
        orderDtos = orderDtos();
        jsonMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.standaloneSetup(controller).setValidator(mock(Validator.class)).build();
    }

    @Test
    void findById() throws Exception {
        when(service.findById(ArgumentMatchers.any()))
                .thenReturn(
                        orderDtos.stream().filter(o -> o.getId() == 1L).findFirst().orElse(null)
                );

        MvcResult mvcResult = mockMvc.perform(
                get("/orders/1").accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(orderDtos.stream().filter(o -> o.getId() == 1L).findFirst().orElse(null)));
    }

    @Test
    void findByClientId() throws Exception {
        when(service.findAll(ArgumentMatchers.any()))
                .thenReturn(
                        orderDtos.stream().filter(o -> o.getClient().getId() == 1L).collect(Collectors.toList())
                );

        MvcResult mvcResult = mockMvc.perform(
                get("/orders/client/1").accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(orderDtos.stream().filter(o -> o.getClient().getId() == 1L).collect(Collectors.toList())));
    }

    @Test
    void findByClientIdPage() throws Exception {
        when(service.findAllPaginated(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(
                        new PageImpl<>(
                                orderDtos.stream().filter(o -> o.getClient().getId() == 1L).collect(Collectors.toList()).subList(0, 2),
                                PageRequest.of(0, 2),
                                orderDtos.size())
                );

        MvcResult mvcResult = mockMvc.perform(
                get("/orders/client/1/paginated").accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(
                        new PageImpl<>(
                                orderDtos.stream().filter(o -> o.getClient().getId() == 1L).collect(Collectors.toList()).subList(0, 2),
                                PageRequest.of(0, 2),
                                orderDtos.size()
                        )
                ));
    }

    @Test
    void saveClient() throws Exception {
        OrderDto orderDto = new OrderDto(
                0L,
                Date.from(LocalDate.now().plusDays(9).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()),
                50,
                null,
                new ClientDto(
                        0L,
                        "Zakariyae",
                        new AddressDto(0L, "5 rue besson", 94110, "Arcueil", "France")
                )
        );

        orderDto.setId(null);
        orderDto.getClient().setId(null);
        orderDto.getClient().getAddress().setId(null);

        when(service.saveOrUpdate(ArgumentMatchers.any()))
                .thenReturn(
                        orderDtos.stream().filter(o -> o.getId() == 1L).findFirst().orElse(null)
                );

        MvcResult mvcResult = mockMvc.perform(
                post("/orders/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(orderDto))
                        .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString())
                .isEqualTo(jsonMapper.writeValueAsString(orderDtos.stream().filter(o -> o.getId() == 1L).findFirst().orElse(null)));
    }

    List<OrderDto> orderDtos() {
        List<OrderDto> list = new ArrayList<>();

        list.add(new OrderDto(1L, new Date(), 3, 2.5, new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France"))));
        list.add(new OrderDto(2L, new Date(), 3, 2.5, new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France"))));
        list.add(new OrderDto(3L, new Date(), 3, 2.5, new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France"))));
        list.add(new OrderDto(4L, new Date(), 3, 2.5, new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France"))));

        return list;
    }

}
