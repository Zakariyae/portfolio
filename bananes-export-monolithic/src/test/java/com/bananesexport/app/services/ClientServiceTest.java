package com.bananesexport.app.services;

import static com.bananesexport.app.specifications.ClientSpecification.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.bananesexport.app.domains.Address;
import com.bananesexport.app.domains.Client;
import com.bananesexport.app.dtos.AddressDto;
import com.bananesexport.app.dtos.ClientDto;
import com.bananesexport.app.repositories.ClientRepository;
import com.bananesexport.app.utils.Mapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
class ClientServiceTest {

    @Mock
    private Mapper mapper;

    @Mock
    private ClientRepository repository;

    @InjectMocks
    private ClientService service;

    private List<Client> clients;
    private List<ClientDto> clientDtos;

    @BeforeEach
    void init() {
        clients = clients();
        clientDtos = clientDtos();
    }

    @Test
    void findByIdTest() {
        when(repository.findOne(ArgumentMatchers.any()))
                .thenReturn(
                        Optional.of(
                                clients.stream().filter(c -> c.getId() == 1L).findFirst()
                        ).orElse(null)
                );

        when(mapper.map(ArgumentMatchers.any(Client.class), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return SimpleMapper.simpleDtoMapper((Client) args[0]);
                });

        ClientDto clientDto = service.findById(clientJoinSpecification().and(clientIdSpecification(1L)));

        assertThat(clientDto.getId()).isSameAs(1L);
    }

    @Test
    void saveOrUpdateTest() {
        ClientDto clientDto = new ClientDto(0L, "Zakariyae", new AddressDto(0L, "5 rue besson", 94110, "Arcueil", "France"));

        clientDto.setId(null);
        clientDto.getAddress().setId(null);

        when(repository.save(ArgumentMatchers.any(Client.class)))
                .thenReturn(
                        clients.stream().filter(c -> c.getId() == 1L).findFirst().orElse(null)
                );

        when(mapper.map(ArgumentMatchers.any(ClientDto.class), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return SimpleMapper.simpleDomMapper((ClientDto) args[0]);
                });

        when(mapper.map(ArgumentMatchers.any(Client.class), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return SimpleMapper.simpleDtoMapper((Client) args[0]);
                });

        ClientDto savedClient = service.saveOrUpdate(clientDto);

        assertThat(savedClient.getId()).isEqualTo(1L);
    }

    @Test
    void findAllTest() {
        when(repository.findAll(clientJoinSpecification()))
                .thenReturn(clients);

        when(mapper.mapCollection(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return SimpleMapper.simpleDtosMapper((List<Client>) args[0]);
                });

        List<ClientDto> list = service.findAll(clientJoinSpecification());

        assertThat(list.stream().map(ClientDto::getId).collect(Collectors.toSet()))
                .containsExactlyElementsOf(clientDtos.stream().map(ClientDto::getId).collect(Collectors.toSet()));
    }

    @Test
    void findAllPaginatedTest() {
        when(repository.findAll(clientJoinSpecification(), PageRequest.of(0, 2)))
                .thenReturn(
                        new PageImpl<>(clients.subList(0, 2), PageRequest.of(0, 2), clients.size())
                );

        when(mapper.mapCollection(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return SimpleMapper.simpleDtosMapper((List<Client>) args[0]);
                });

        Page<ClientDto> list = service.findAllPaginated(clientJoinSpecification(), PageRequest.of(0, 2));

        assertThat(list.stream().map(ClientDto::getId).collect(Collectors.toSet()))
                .containsExactlyElementsOf(clientDtos.subList(0, 2).stream().map(ClientDto::getId).collect(Collectors.toSet()));
    }

    List<Client> clients() {
        List<Client> list = new ArrayList<>();

        list.add(new Client(1L, "Zakariyae", new Address(1L, "5 rue besson", 94110, "Arcueil", "France")));
        list.add(new Client(2L, "Kamel", new Address(2L, "10 rue besson", 60000, "Oujda", "Maroc")));
        list.add(new Client(3L, "Oualid", new Address(3L, "15 rue besson", 68000, "Oujda", "Maroc")));
        list.add(new Client(4L, "Amine", new Address(4L, "20 rue besson", 75001, "Paris", "France")));

        return list;
    }

    List<ClientDto> clientDtos() {
        List<ClientDto> list = new ArrayList<>();

        list.add(new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France")));
        list.add(new ClientDto(2L, "Kamel", new AddressDto(2L, "10 rue besson", 60000, "Oujda", "Maroc")));
        list.add(new ClientDto(3L, "Oualid", new AddressDto(3L, "15 rue besson", 68000, "Oujda", "Maroc")));
        list.add(new ClientDto(4L, "Amine", new AddressDto(4L, "20 rue besson", 75001, "Paris", "France")));

        return list;
    }

}
