package com.bananesexport.app.services;

import static com.bananesexport.app.specifications.OrderSpecification.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.bananesexport.app.domains.Address;
import com.bananesexport.app.domains.Client;
import com.bananesexport.app.domains.Order;
import com.bananesexport.app.dtos.AddressDto;
import com.bananesexport.app.dtos.ClientDto;
import com.bananesexport.app.dtos.OrderDto;
import com.bananesexport.app.repositories.OrderRepository;
import com.bananesexport.app.utils.Mapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest
class OrderServiceTest {

    @Mock
    private Mapper mapper;

    @Mock
    private OrderRepository repository;

    @InjectMocks
    private OrderService service;

    private List<Order> orders;
    private List<OrderDto> orderDtos;

    @BeforeEach
    void init() {
        orders = orders();
        orderDtos = orderDtos();
    }

    @Test
    void findByIdTest() {
        when(repository.findOne(ArgumentMatchers.any()))
                .thenReturn(
                        Optional.of(
                                orders.stream().filter(o -> o.getId() == 1L).findFirst()
                        ).orElse(null)
                );

        when(mapper.map(ArgumentMatchers.any(Order.class), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return simpleDtoMapper((Order) args[0]);
                });

        OrderDto orderDto = service.findById(orderJoinSpecification().and(orderIdSpecification(1L)));

        assertThat(orderDto.getId()).isSameAs(1L);
    }

    @Test
    void saveOrUpdateTest() {
        OrderDto orderDto = new OrderDto(1L, new Date(), 3, 2.5, new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France")));

        orderDto.setId(null);
        orderDto.getClient().setId(null);
        orderDto.getClient().getAddress().setId(null);

        when(repository.save(ArgumentMatchers.any(Order.class)))
                .thenReturn(
                        orders.stream().filter(o -> o.getId() == 1L).findFirst().orElse(null)
                );

        when(mapper.map(ArgumentMatchers.any(OrderDto.class), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return simpleDomMapper((OrderDto) args[0]);
                });

        when(mapper.map(ArgumentMatchers.any(Order.class), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return simpleDtoMapper((Order) args[0]);
                });

        OrderDto savedOrder = service.saveOrUpdate(orderDto);

        assertThat(savedOrder.getId()).isEqualTo(1L);
    }

    @Test
    void findAllTest() {
        when(repository.findAll(orderJoinSpecification()))
                .thenReturn(orders);

        when(mapper.mapCollection(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return simpleDtosMapper((List<Order>) args[0]);
                });

        List<OrderDto> list = service.findAll(orderJoinSpecification());

        assertThat(list.stream().map(OrderDto::getId).collect(Collectors.toSet()))
                .containsExactlyElementsOf(orderDtos.stream().map(OrderDto::getId).collect(Collectors.toSet()));
    }

    @Test
    void findAllPaginatedTest() {
        when(repository.findAll(orderJoinSpecification(), PageRequest.of(0, 2)))
                .thenReturn(
                        new PageImpl<>(orders.subList(0, 2), PageRequest.of(0, 2), orders.size())
                );

        when(mapper.mapCollection(ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenAnswer(invocation -> {
                    Object[] args = invocation.getArguments();
                    return simpleDtosMapper((List<Order>) args[0]);
                });

        Page<OrderDto> list = service.findAllPaginated(orderJoinSpecification(), PageRequest.of(0, 2));

        assertThat(list.stream().map(OrderDto::getId).collect(Collectors.toSet()))
                .containsExactlyElementsOf(orderDtos.subList(0, 2).stream().map(OrderDto::getId).collect(Collectors.toSet()));
    }

    Order simpleDomMapper(OrderDto orderDto) {
        return new Order(
                orderDto.getId(),
                orderDto.getDeliveryDate(),
                orderDto.getQuantity(),
                //orderDto.getPrice(),
                SimpleMapper.simpleDomMapper(orderDto.getClient())
        );
    }

    OrderDto simpleDtoMapper(Order order) {
        return new OrderDto(
                order.getId(),
                order.getDeliveryDate(),
                order.getQuantity(),
                order.getQuantity() * 2.5D,
                SimpleMapper.simpleDtoMapper(order.getClient())
        );
    }

    List<OrderDto> simpleDtosMapper(List<Order> order) {
        List<OrderDto> list = new ArrayList<>();

        order.forEach(o -> list.add(simpleDtoMapper(o)));

        return list;
    }

    List<Order> orders() {
        List<Order> list = new ArrayList<>();

        list.add(new Order(1L, new Date(), 3, /*7.5,*/ new Client(1L, "Zakariyae", new Address(1L, "5 rue besson", 94110, "Arcueil", "France"))));
        list.add(new Order(2L, new Date(), 5, /*7.5,*/ new Client(2L, "Kamel", new Address(2L, "10 rue besson", 60000, "Oujda", "Maroc"))));
        list.add(new Order(3L, new Date(), 7, /*7.5,*/ new Client(3L, "Oualid", new Address(3L, "15 rue besson", 68000, "Oujda", "Maroc"))));
        list.add(new Order(4L, new Date(), 9, /*7.5,*/ new Client(4L, "Amine", new Address(4L, "20 rue besson", 75001, "Paris", "France"))));

        return list;
    }

    List<OrderDto> orderDtos() {
        List<OrderDto> list = new ArrayList<>();

        list.add(new OrderDto(1L, new Date(), 3, 7.5, new ClientDto(1L, "Zakariyae", new AddressDto(1L, "5 rue besson", 94110, "Arcueil", "France"))));
        list.add(new OrderDto(2L, new Date(), 3, 7.5, new ClientDto(2L, "Kamel", new AddressDto(2L, "10 rue besson", 60000, "Oujda", "Maroc"))));
        list.add(new OrderDto(3L, new Date(), 3, 7.5, new ClientDto(3L, "Oualid", new AddressDto(3L, "15 rue besson", 68000, "Oujda", "Maroc"))));
        list.add(new OrderDto(4L, new Date(), 3, 7.5, new ClientDto(4L, "Amine", new AddressDto(4L, "20 rue besson", 75001, "Paris", "France"))));

        return list;
    }

}
