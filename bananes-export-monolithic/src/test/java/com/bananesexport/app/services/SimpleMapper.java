package com.bananesexport.app.services;

import com.bananesexport.app.domains.Address;
import com.bananesexport.app.domains.Client;
import com.bananesexport.app.dtos.AddressDto;
import com.bananesexport.app.dtos.ClientDto;

import java.util.ArrayList;
import java.util.List;

class SimpleMapper {

    private SimpleMapper() {
    }

    static Client simpleDomMapper(ClientDto clientDto) {
        return new Client(
                clientDto.getId(),
                clientDto.getName(),
                new Address(
                        clientDto.getAddress().getId(),
                        clientDto.getAddress().getStreet(),
                        clientDto.getAddress().getZipCode(),
                        clientDto.getAddress().getCity(),
                        clientDto.getAddress().getCountry()
                )
        );
    }

    static ClientDto simpleDtoMapper(Client client) {
        return new ClientDto(
                client.getId(),
                client.getName(),
                new AddressDto(
                        client.getAddress().getId(),
                        client.getAddress().getStreet(),
                        client.getAddress().getZipCode(),
                        client.getAddress().getCity(),
                        client.getAddress().getCountry()
                )
        );
    }

    static List<ClientDto> simpleDtosMapper(List<Client> client) {
        List<ClientDto> clientDtos = new ArrayList<>();

        client.forEach(c -> clientDtos.add(simpleDtoMapper(c)));

        return clientDtos;
    }

}
